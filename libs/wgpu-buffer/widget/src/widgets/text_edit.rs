// This file is part of Nitrogen.
//
// Nitrogen is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Nitrogen is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Nitrogen.  If not, see <http://www.gnu.org/licenses/>.
use crate::{
    font_context::{FontContext, FontId, SANS_FONT_ID},
    paint_context::PaintContext,
    region::{Extent, Position, Region},
    text_run::TextRun,
    widget::{Widget, WidgetFocus},
    widget_info::WidgetInfo,
};
use anyhow::Result;
use csscolorparser::Color;
use gpu::Gpu;
use input::InputEvent;
use parking_lot::RwLock;
use runtime::ScriptHerder;
use std::{collections::VecDeque, sync::Arc, time::Instant};
use window::{
    size::{AbsSize, LeftBound, RelSize, Size},
    Window,
};

#[derive(Debug)]
pub struct TextEdit {
    lines: VecDeque<TextRun>,
    read_only: bool,
    default_color: Color,
    default_font: FontId,
    default_size: Size,

    measured_extent: Extent<AbsSize>,
    layout_position: Position<Size>,
    layout_extent: Extent<Size>,
}

impl TextEdit {
    pub fn new(markup: &str) -> Self {
        let mut obj = Self {
            lines: VecDeque::new(),
            read_only: true, // NOTE: writable text edits not supported yet.
            default_color: Color::from([0, 0, 0]),
            default_font: SANS_FONT_ID,
            default_size: Size::from_pts(12.),

            measured_extent: Extent::zero(),
            layout_position: Position::origin(),
            layout_extent: Extent::zero(),
        };
        obj.replace_content(markup);
        obj
    }

    pub fn with_default_color(mut self, color: &Color) -> Self {
        self.default_color = color.to_owned();
        self
    }

    pub fn with_default_font(mut self, font_id: FontId) -> Self {
        self.default_font = font_id;
        self
    }

    pub fn with_default_size(mut self, size: Size) -> Self {
        self.default_size = size;
        self
    }

    pub fn default_color(&self) -> &Color {
        &self.default_color
    }

    pub fn default_font(&self) -> FontId {
        self.default_font
    }

    pub fn default_size(&self) -> Size {
        self.default_size
    }

    pub fn set_font_size(&mut self, size: Size) {
        for line in &mut self.lines {
            line.set_default_size(size);
            line.select_all();
            line.change_size(size);
            line.select_none();
        }
        self.default_size = size;
    }

    pub fn with_text(mut self, text: &str) -> Self {
        self.replace_content(text);
        self
    }

    pub fn wrapped(self) -> Arc<RwLock<Self>> {
        Arc::new(RwLock::new(self))
    }

    pub fn replace_content(&mut self, markup: &str) {
        let lines = markup
            .split('\n')
            .map(|markup| self.make_run(markup))
            .collect::<VecDeque<TextRun>>();
        self.lines = lines;
    }

    pub fn append_line(&mut self, markup: &str) {
        self.lines.push_back(self.make_run(markup));
    }

    pub fn last_line_mut(&mut self) -> Option<&mut TextRun> {
        self.lines.back_mut()
    }

    pub fn line_count(&self) -> usize {
        self.lines.len()
    }

    pub fn remove_first_line(&mut self) {
        self.lines.remove(0);
    }

    fn make_run(&self, text: &str) -> TextRun {
        TextRun::empty()
            .with_hidden_selection()
            .with_default_size(self.default_size)
            .with_default_color(&self.default_color)
            .with_default_font(self.default_font)
            .with_text(text)
    }
}

impl Widget for TextEdit {
    fn measure(&self, win: &Window, font_context: &FontContext) -> Result<Extent<Size>> {
        let mut width = AbsSize::zero();
        let mut height_offset = AbsSize::zero();
        let line_count = self.lines.len();
        for (i, line) in self.lines.iter().enumerate() {
            let span_metrics = line.measure(win, font_context)?;
            if i != line_count - 1 {
                height_offset += span_metrics.line_gap;
            }
            height_offset += span_metrics.height;
            width = width.max(&span_metrics.width);
        }
        let measured_extent = Extent::new(width, height_offset);
        Ok(measured_extent.into())
    }

    // fn layout(
    //     &mut self,
    //     _now: Instant,
    //     region: Region<RelSize>,
    //     _win: &Window,
    //     _font_context: &mut FontContext,
    // ) -> Result<()> {
    //     self.layout_position = *region.position();
    //     self.layout_extent = *region.extent();
    //     Ok(())
    // }

    fn upload(
        &self,
        _now: Instant,
        win: &Window,
        gpu: &Gpu,
        context: &mut PaintContext,
    ) -> Result<()> {
        let info = WidgetInfo::default();
        let widget_info_index = context.push_widget(&info);

        let mut pos = self.layout_position.as_abs(win);
        *pos.bottom_mut() += self.measured_extent.height();
        let line_count = self.lines.len();
        for (i, line) in self.lines.iter().enumerate() {
            let line_metrics = line.measure_cached();
            // TODO: aabb bounds check against the cached region
            if pos.bottom() < AbsSize::Px(0f32)
                && pos.bottom() - line_metrics.height < AbsSize::Px(0f32)
            {
                continue;
            }
            *pos.bottom_mut() -= line_metrics.height;
            line.upload(pos.into(), widget_info_index, win, gpu, context)?;
            if i != line_count - 1 {
                *pos.bottom_mut() -= line_metrics.line_gap;
            }
        }

        Ok(())
    }

    fn handle_event(
        &mut self,
        _event: &InputEvent,
        _focus: WidgetFocus,
        _cursor_position: Position<AbsSize>,
        _herder: &mut ScriptHerder,
    ) -> Result<()> {
        assert!(self.read_only);
        Ok(())
    }
}
